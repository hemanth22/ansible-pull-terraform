# ansible-pull-terraform

This code is designed to perform ansible pull and provision the terraform to centos system.

# How to execute.

Create below file.  

vi main.yml
```
---
- hosts: servers
  remote_user: centos
  become: true
  become_method: sudo
  gather_facts: no
  tasks:
    - name: Install pip on the servers
      yum:
        name: python-pip
        state: latest
        update_cache: true
      become: true

    - name: Install pip3 on the servers
      yum:
        name: python3-pip
        state: latest        
      become: true

    - name: Ensure ansible is installed on servers
      pip:
        name: ansible

    - name: Executing ansible-pull on servers
      command: ansible-pull -U https://gitlab.com/hemanth22/ansible-pull-terraform.git

```

Create below inventory file.  

vi inventory
```
[servers]
34.221.246.71
[servers:vars]
ansible_python_interpreter=/usr/bin/python
ansible_ssh_private_key_file=~/.ssh/bitroid.pem
```

vi ansible.cfg
```
[defaults]
log_path = /var/log/ansible.log
command_warnings = False
system_warnings = False
action_warnings = False
```

```
.
├── ansible.cfg
├── inventory
├── main.yml
```
Execute below command if private key details addd in inventory file.  

`ansible-playbook main.yml -i inventory`

Execute below command if private key is not mentioned in inventory file.  

`ansible-playbook main.yml --private-key ~/.ssh/bitroid.pem -i inventory`

